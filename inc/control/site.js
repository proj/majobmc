module.exports = function(db) {

var dir_view = __dirname + '/../view/site/';

return {
	get_home: function(req, res) {
		if (req.session.user) {
			res.render(dir_view + 'home_on.html', {
				user: req.session.user
			});
		} else {
			res.render(dir_view + 'home_off.html');
		}
	},

	get_play: function(req, res) {
		if (!req.session.user)
			res.redirect('/login');

		res.render(dir_view + 'play.html', {
			user: req.session.user
		});
	},

	get_login: function(req, res) {
		if (req.session.user)
			res.redirect('/');

		var errors = [];

		if (req.session.errors) {
			errors = req.session.errors;
			delete req.session.errors;
		}

		res.render(dir_view + 'login.html', {
			errors: errors
		});
	},

	post_login: function(req, res) {
		if (req.session.user)
			res.redirect('/');

		var post = req.body;

		db.query('select * from user_info where username = ' + db.escape(post.username) + ' and passwd = ' + db.escape(post.passwd) + ';', function(err, result) {
			if (err) {
				console.log('Erreur lors de la connexion : ' + err);
				return;
			}

			if (result[0]) {
				req.session.user = result[0];
				res.redirect('/');
			} else {
				req.session.errors = [ 'Erreur lors de la connexion : utilisateur inexistant' ];
				res.redirect('/login');
			}
		});
	},

	get_register: function(req, res) {
		if (req.session.user)
			res.redirect('/');

		var errors = [];

		if (req.session.errors) {
			errors = req.session.errors;
			delete req.session.errors;
		}

		res.render(dir_view + 'register.html', {
			errors: errors
		});
	},

	post_register: function(req, res) {
		if (req.session.user)
			res.redirect('/');

		var post = req.body;

		db.query('select * from user_info where username = ' + db.escape(post.username) + ';', function(err, result) {
			if (result[0]) {
				req.session.errors = [ 'Erreur lors de l\'inscription : pseudo déjà utilisé' ];
				res.redirect('/register');
			} else {
				db.query('insert into user_info(username, passwd, email, id_cur_room) ' +
				'values(' + db.escape(post.username) + ', ' + db.escape(post.passwd) + ', \'email\', 1);', function(err, result) {
					if (err) {
						console.log('Erreur lors de l\'inscription : ' + err);
						return;
					}

					res.redirect('/login');
				});
			}
		});
	},

	get_logout: function(req, res) {
		if (!req.session.user)
			res.redirect('/login');

		delete req.session.user;

		res.redirect('/');
	},

	get_socrate: function(req, res) {
		if (!req.session.user)
			res.redirect('/');

		res.render(dir_view + 'socrate.html', {
			user: req.session.user
		});
	}
};

};

  __  __       _     ___ __  __  ___ 
 |  \/  |__ _ (_)___| _ )  \/  |/ __|
 | |\/| / _` || / _ \ _ \ |\/| | (__ 
 |_|  |_\__,_|/ \___/___/_|  |_|\___|
            |__/                     

Le code -aussi bien du client que du serveur- n'est pas propre actuellement car le but est de faire un développement initial rapide. Par la suite le code sera nettoyé.

Todo :
 - Nettoyer le code
 - Classer les questions et réponses (Nom propre ; Couleur ; Pays ; ... ; Autre)
 - Sécuriser les failles SQL
 - Sécuriser les erreurs Javascript qui permettent de tricher
 - Finir l'espace utilisateur
 - Faire un fichier de log
 - Ecrire le reste de la _todolist_
 - Vies (salons) + Log + Ordre des réponses aléatoire

(function() {
	var players = [];
	var log = document.getElementById('log');

	var p = {
		id: document.getElementById('user_id').value,
		username: '',
		cur_answer: null,
		lol: false
	};

	var answers_numbers   = document.querySelectorAll('.answer-number');
	var question_timer    = document.getElementById('question-timer');
	var list_players      = document.getElementById('list-players');
	var question_number   = document.getElementById('question-number');
	var question_question = document.getElementById('question');
	var data_answers      = document.querySelectorAll('[data-answer]');
	var btn_lol           = document.getElementById('btn_lol');
	var btn_answers       = document.querySelectorAll('[data-answer]');

	p.username = Math.round(Math.random() * 50000).toString();
	socket.emit('new_player', p);

	for (var i = 0 ; i < data_answers.length ; i++) {
		data_answers[i].onclick = function() {
			socket.emit('answer', this.getAttribute('data-answer'));
		};
	}

	btn_lol.onclick = function() {
		socket.emit('lol');
	};

	/* ------------------------------
		RECEPTION DES DONNES DU SERVEUR
		------------------------------- */
	// Un joueur ouvre la page
	socket.on('new_player', function(ps, p) {
		players.push(ps);
	});

	// Un joueur quitte la page
	socket.on('bye_player', function(datas) {
	});

	// Un joueur répond à une question
	socket.on('answer', function(r) {
		btn_answers[r.cur_answer].classList.add('active');
	});

	// Un joueur a cliqué sur "MDR"
	socket.on('lol', function(r) {
		btn_lol.style.display = 'none';
	});

	// Résultat de la question
	socket.on('result_answers', function(answers) {
		var i = answers_numbers.length;
		while (i--) {
			answers_numbers[i].style.display = 'inline-block';
		}

		question_timer.style.display = 'none';

		for (var i = 0 ; i < answers_numbers.length ; i++) {
			answers_numbers[i].innerHTML = answers[i];
		}
	});

	// Nouvelle question
	socket.on('new_question', function(question, answers, q_number) {
		var i = answers_numbers.length;
		while (i--) {
			answers_numbers[i].style.display = 'none';
		}

		question_timer.style.display = 'inline';

		question_question.innerHTML = question;

		btn_answers[0].children[0].innerHTML = answers[0];
		btn_answers[1].children[0].innerHTML = answers[1];
		btn_answers[2].children[0].innerHTML = answers[2];

		btn_answers[0].classList.remove('active');
		btn_answers[1].classList.remove('active');
		btn_answers[2].classList.remove('active');

		question_number.innerHTML = q_number;

		btn_lol.style.display = 'inline-block';
	});

	// Mise à jour de la liste des joueurs
	socket.on('update_player_list', function(ps, show_answer) {
		players = ps;

		while (list_players.hasChildNodes())
			list_players.removeChild(list_players.lastChild);

		i = players.length;
		while (i--) {
			var player = players[i];

			var ppp = document.createElement('li');

			if (player.lol)
				ppp.appendChild(document.createTextNode('MDR - '));

			if (player.cur_answer) {
				ppp.appendChild(document.createTextNode(player.username));

				if (show_answer) {
					ppp.classList.add('answer_' + player.cur_answer);
				} else {
					ppp.classList.add('active');
				}
			} else {
				ppp.appendChild(document.createTextNode(player.username));
			}

			list_players.appendChild(ppp);
		}
	});

	// Mise à jour du compte à rebours
	socket.on('update_time_left', function(time_left) {
		question_timer.innerHTML = time_left + 's';
	});
})();

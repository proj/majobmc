create table game_room(
	id integer unsigned not null auto_increment,

	constraint pk_room
		primary key(id)
);

create table user_info(
	id          integer unsigned not null auto_increment,
	username    varchar(22),
	passwd      varchar(128),
	email       varchar(156),
	id_cur_room integer unsigned not null,

	constraint pk_player
		primary key(id),

	constraint fk_player_room
		foreign key(id_cur_room)
		references game_room(id)
);

insert into game_room() values(); -- on créer un premier salon

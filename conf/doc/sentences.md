Un joueur rejoint la partie
===========================
%s a atterri chez les fous !
Un joueur rejoint la partie
Bienvenue à %s. Plus on est de fous, plus on rit !
Un joueur rejoint la partie
Wesh %s, bien ou bien ?
Un joueur rejoint la partie
Grosse soirée d'enjaillement en perspective, avec %s en guest !
Un joueur rejoint la partie
%s a rejoint une partie. Pourquoi, il l'avait déjà jointe ? #grossesbarres
Bienvenue %s ! Vous êtes notre millionième visiteur ! <a href="lien troll">Obtenir votre lot</a>

Un joueur quitte la partie 
==========================
%s a fui devant tant de beau monde.
Un joueur quitte la partie
%s a détalé comme un lapin. Quel lâche !
Un joueur quitte la partie
%s s'en est allé dans le beau monde de l'IRL.
Un joueur quitte la partie
%s a visiblement souhaité s'adonner à une activité plus saine...
Un joueur quitte la partie
/ragequit %s

En attendant le démarrage
=========================
%s a été le premier à rejoindre cette partie. Je m'en souviens comme si c'était hier ! Ah ben, euh... c'était il y a n secondes...
En attendant le démarrage (joueur aléatoire)
Bande d'impatients... Faites comme %s, il attend sagement, lui !
En attendant le démarrage
Le dernier joueur à nous avoir rejoint est %s. Mais vous le saviez déjà, il est inscrit dans les logs juste en-dessous...
En attendant le démarrage
Cette partie va être épique ! Avec un joueur comme %s, le jeu s'annonce prenant \o/

MDR alone
=========
-

MDR à bcp (25 ~ 30)
===================
-

Unanimité
=========
-

SCT
===
-

Egalité
=======
-

MDR en bande (private joke) (5 ~ 6)
===================================
-

/* Bibliothèques/Frameworks
	-------------------------- */
var express = require('express'),
	app             = express(),
	bodyParser      = require('body-parser'),
	cookieParser    = require('cookie-parser'),
	cookieSession   = require('cookie-session'),
	expressSession  = require('express-session'),
	methodOverride  = require('method-override'),
	server          = require('http').Server(app),
	io              = require('socket.io').listen(server),
	fs              = require('fs'),
	ent             = require('ent'),
	swig            = require('swig');

/* Bibliothèques personnelles
	---------------------------- */
var db = require('./conf/db').connection;

db.connect(function(err) {
	if (err)
		throw 'Erreur de connexion à la base de données : ' + err;

	/* On précise pleins d'informations sur l'application
		---------------------------------------------------- */
	app
		.engine('html', swig.renderFile)
		.set('view engine', 'html')
		.set('views', __dirname + '/inc/view/')
		.set('view cache', false)
		.use(methodOverride())
		.use(express.static(__dirname + '/web'))
		.use(cookieParser('trowwswagmajobmc<3'))
		.use(bodyParser.json())
		.use(bodyParser.urlencoded({ extended: true }))
		.use(cookieSession({
			keys: [ 'trowwswagmajobmc<3x2' ]
		}))
		.use(expressSession({
			secret: 'trowwswagmajobmc<3x3',
			saveUninitialized: true,
			resave: true
		}));

	/* Récupération de pleins d'informations utiles
		---------------------------------------------- */
	var TIME_QUESTION = 8,
		TIME_ANSWER = 6,

		players = [],
		questions = [],
		answers = [],
		cur_party = {
			time_left: 0,
			state: 'question'
		},
		question_number = 1;

	fs.readFile(__dirname + '/conf/data.js', 'utf8', function (err, data) {
		if (err)
			throw 'Erreur lors du chargement des questions/réponses : ' + err;

		data = JSON.parse(data);

		questions = data.questions;
		answers   = data.answers;
	});

	/* On créer les belles routes
		---------------------------- */
	var site = require('./inc/control/site')(db);

	app
		.get('/', site.get_home)
		.get('/play', site.get_play)
		.get('/login', site.get_login)
		.get('/register', site.get_register)
		.get('/logout', site.get_logout)
		.get('/socrate', site.get_socrate)
		.post('/login', site.post_login)
		.post('/register', site.post_register);

	/* Tous les trucs qui concernent les sockets
		------------------------------------------- */
	io.set('transports', [ 'websocket', 'polling' ]);

	io.sockets.on('connection', function(socket) {
		// Connexion d'un joueur
		// - on encode son pseudo
		// - on enregistre son ses informations dans son socket personnel
		// - on précise qu'un nouveau joueur est arrivé
		// - on ajoute le joueur à la liste des joueurs
		// - on met à jour la liste des joueurs
		socket.on('new_player', function(datas) {
			db.query('select * from user_info where id = \'' + datas.id + '\';', function(err, result) {
				if (err) {
					console.log('Erreur lors de l\'ajout d\'un nouveau joueur : ' + err);
					return;
				}

				if (!result[0]) {
					console.log('Erreur lors de l\'ajout d\'un nouveau joueur : aucun résultat');
					return;
				}

				datas.username = ent.encode(result[0].username);
				socket.player = datas;

				players.push(datas);

				io.sockets.emit('new_player', players, datas);
				io.sockets.emit('update_player_list', players);
			});
		});

		// Déconnexion d'un joueur
		// - on récupère ses informations dans son socket personnel
		// - on précise que le joueur est parti
		// - on met à jour la liste des joueurs
		socket.on('disconnect', function() {
			var i = players.length;
			var player = socket.player;

			if (player != null) {
				if (typeof player.username != 'undefined') {
					while (i--) {
						if (players[i] == player)
							players.splice(i, 1);
					}

					socket.broadcast.emit('bye_player', player);
					io.sockets.emit('update_player_list', players);
				}
			}
		});

		// Réponse d'un joueur
		// - on récupère les informations du joueur dans son socket personnel
		// - on récupère sa réponse
		// - on met à jour la réponse du joueur
		// - on met à jour la liste des joueurs
		socket.on('answer', function(answer) {
			var player = socket.player;

			if (player.cur_answer == null && cur_party.state == 'question') {
				var p = null;

				var i = players.length;
				while (i--) {
					if (player == players[i])
						p = players[i];
				}

				if (answer != '0' && answer != '1' && answer != '2')
					answer = '1';

				player.cur_answer = answer;
				p.cur_answer = answer;

				socket.emit('answer', player);

				io.sockets.emit('update_player_list', players);
			}
		});

		// "MDR" d'un joueur
		// - on récupère les informations du joueur dans son socket
		// - on met à jour l'état de son "MDR"
		// - on précise que le joueur a rigoler vraiment très beaucoup
		// - on met à jour la liste des joueurs
		socket.on('lol', function() {
			var player = socket.player;

			if (player.lol == false) {
				var p = null;

				var i = players.length;
				while (i--) {
					if (player == players[i])
						p = players[i];
				}

				player.lol = true;
				p.lol = true;

				socket.emit('lol', player);

				io.sockets.emit('update_player_list', players);
			}
		});
	});

	/* Là on fait pleins de trucs de fou, chaque seconde
		--------------------------------------------------- */
	setInterval(function() {
		cur_party.time_left--;

		if (cur_party.time_left <= 0) {
			switch (cur_party.state) {
				case 'question':
					cur_party.state = 'answer';
					break;

				case 'answer':
					cur_party.state = 'question';
					break;
			}

			switch (cur_party.state) {
				case 'question':
					question_number++;
					cur_party.time_left = TIME_QUESTION;

					var q  = questions[Math.floor(Math.random() * questions.length)];

					var a1 = answers[Math.floor(Math.random() * answers.length)];
					var a2 = answers[Math.floor(Math.random() * answers.length)];
					var a3 = answers[Math.floor(Math.random() * answers.length)];

					var i = players.length;
					while (i--) {
						players[i].cur_answer = null;
						players[i].lol = false;
					}

					io.sockets.emit('new_question', q, [ a1, a2, a3 ], question_number);
					break;

				case 'answer':
					cur_party.time_left = TIME_ANSWER;

					var ans = [ 0, 0, 0 ];

					var i = players.length;
					while (i--) {
						if (players[i].cur_answer == '0')
							ans[0]++;
						else if (players[i].cur_answer == '1')
							ans[1]++;
						else if (players[i].cur_answer == '2')
							ans[2]++;
					}

					io.sockets.emit('result_answers', ans);
					break;
			}

			io.sockets.emit('update_player_list', players, true);
		}

		io.sockets.emit('update_time_left', cur_party.time_left);
	}, 1000);

	/* On lance le serveur en écoutant le port 8080
		---------------------------------------------- */
	server.listen(8080);
});
